
export function isLoggedIn() {
    const user = localStorage.getItem("user");
    if (user === null){
        return false
    } else 
        return true
}

export function login(user) {
    localStorage.setItem("user", JSON.stringify(user));
}   

export function logout(){
    localStorage.clear();
}

export function getUser(){
    const user = JSON.parse(localStorage.getItem("user"));
    return user;
}