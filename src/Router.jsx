import {BrowserRouter, Route, Switch} from "react-router-dom";
import React, {Component} from 'react';
import './App.css';
import {Container} from "semantic-ui-react";
import App from './App.js';
import MenuBar from './MenuBar.jsx';
import CreateRecipe from './CreateRecipe.jsx';
//import InitialRecipe from './InitialRecipe.jsx';
import BannerImg from './banner2.png';
import RecipeLayout from './RecipeLayout';
import Login from "./Login.jsx";
import RecipeEdior from "./RecipeEditor";
import Contact from './Contact.jsx';
//import {isLoggedIn} from "./auth.js"

/*const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      console.log(props, isLoggedIn());
      return isLoggedIn() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )}
    }
  />
);
*/

class Router extends Component {
  state = {
    recipes: [
      {
        name: 'pizza-prosciutto-e-funghi',
        ing: ['200g Pizzateig', '70ml Tomatenssauce', '70g Mozarella, gerieben', '60g Kochschinken', '40g Champignonscheiben'],
        prep: ['Drehe den Teigling aus und verteile 70ml Tomatensauce gleichmäßig von innen nach außen darauf, sodass 1cm Rand frei bleibt. Gib 70 g Mozarella gleichmäßig von außen nach innen gebröselt darüber, sodass die Pizza bis auf 1cm Rand gleichmäßig bedeckt ist.', 'Verteile dann 60g geviertelten Kochschinken und 40g Champignons auf der ganzen Pizza. Schiebe die Pizza in den ofen und wende sie mindestens einmal, sodass sie gleichmäßig backt und bräunt.', 'Wenn die Pizza fertig gebacken ist, platziere die Pizza auf einen Telle und scheide sie in 8 gleich große Stücke.'],
        slug: 'pizza-prosciutto-e-funghi',
        ratings: {'pawel':5},
        user: 'pawel',
        img: 'http://limon.ch/wp-content/uploads/2013/10/proscuittofunghi.jpg'
      }
    ]
  }

  addItemToList = (item) => {
    this.setState({
      recipes: [
        ...this.state.recipes,
        item
      ]
    });
  }

  updateRecipe = recipe => {
    const recipes = this.state.recipes.slice();
    const idx = recipes.findIndex(r => r.name === recipe.name);
    if (idx !== -1) {
      recipes[idx] = recipe
    }

    this.setState({ recipes })
  }

  render() {
    // https://github.com/ReactTraining/react-router/issues/4105
    const AppWithProps = props => (
      <App recipes={this.state.recipes} updateRecipe={this.updateRecipe} {...props}/>
    );

    const CreateRecipeWithProps = props => (
      <CreateRecipe addItemToList={this.addItemToList} {...props}/>
    );

    const LayoutWithProps = props => (
      <RecipeLayout recipes={this.state.recipes} {...props} />
    );

    const RecipeEditorWithProps = props => (
      <RecipeEdior recipes={this.state.recipes} updateRecipe={this.updateRecipe} {...props} />
    );

    return (
      <BrowserRouter>
        <div>
          <div>
            <img className="banner" src={BannerImg} alt="banner"/> {/*<h1 className="bannerText centered">Fap iano</h1>*/}
          </div>
          <Container
            className="container"
            style={{
            paddingTop: "24px"
          }}>
            <MenuBar/>
            <Switch>
              <Route exact path="/" render={AppWithProps}/>
              <Route path="/your-recipes" render={AppWithProps} />
              <Route path="/best-recipes" render={AppWithProps} />
              <Route path="/create-recipe" render={CreateRecipeWithProps} />
              {/*<Route path="/recipe/pizza-prosciutto-e-funghi" component={InitialRecipe}/>*/}
              <Route path="/recipe/edit/:name" render={RecipeEditorWithProps} />
              <Route path="/recipe/:name" render={LayoutWithProps} />
              <Route path="/login" component={Login}/>
              <Route path="/contact" component={Contact} />
            </Switch>
          </Container>
        </div>
      </BrowserRouter>
    );
  }
}

export default Router;