import React, {Component} from 'react';
import './App.css';
import {Grid, Segment, Image, Button} from "semantic-ui-react";
import { Link } from "react-router-dom";
import DefaultImage from './food-placeholder.png';
import { getUser } from "./auth"

export const findRecipe = (list, name) => {
  const result = list.filter(r => r.slug === name)
  return result.length >= 1 ? result[0] : null
};

class RecipeLayout extends Component {
  render() {
    const name = this.props.match.params.name;
    const recipe = findRecipe(this.props.recipes, name);
    if (!recipe) {
      return <p>No recipe with name {name} found</p>;
    }


    const user = getUser();
    // const userName = user && user.name;
    // user == null
    // null.name
    // short circuit, truthy falsy checken
    const userName = user && user.name;
    const showEditButton = userName === recipe.user;

    return (
      <div>
        <Segment inverted color="red">
        <Grid>
          <Grid.Row>
            <Grid.Column textAlign="center" width={8} style={{
              marginLeft: 28
            }}>
              <h1>{recipe.name}</h1>
            </Grid.Column >
            <Grid.Column textAlign="center" width={4}>
              <h2>von {recipe.user} </h2>
            </Grid.Column>
            <Grid.Column width={2}>
            {showEditButton && <Button color="green" as={Link} to={`/recipe/edit/${recipe.name}`}>Edit</Button>}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column mobile={16} computer={8}>
              <Segment
                inverted
                color="yellow"
                style={{
                marginLeft: 28
              }}
                className="imageSegment">
                <Image className="imagewidth"  src={recipe.img !== '' ? recipe.img : DefaultImage}/>
              </Segment>
            </Grid.Column>
            <Grid.Column style={{
              paddingLeft:40
              }} mobile={16} computer={8}>
              <Segment inverted color="black">
                <h2>Zutaten:</h2>
                <ul>
                  {recipe.ing.map((item, idx) => <li key={idx}>{item}</li>)}
                </ul>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Segment
                inverted
                color="black"
                style={{
                marginLeft: 28
              }}>
                <h2>Zubereitung:</h2>
                <ol>
                  {recipe.prep.map((item, idx) => <li key={idx}>{item}</li>)}
                </ol>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
      </div>
    );
  }
}


export default RecipeLayout;