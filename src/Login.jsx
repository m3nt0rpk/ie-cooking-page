import React, {Component} from 'react';
import './App.css';
import {Grid, Segment, Form, Button, Divider} from "semantic-ui-react";
import {login, logout} from "./auth.js"

import {users} from './App'

class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: '',
      password: '',
      regisUser: '',
      regisPassword: '',
      error: false,
    };
    //wenn nicht mit ES6 syntax this.handleChangeUser = this.handleChangeUser.bind(this)
  }

/* handleChangeUser(e){
    this.setState({user: e.target.value})
}*/ 

  handleChangeUser = (e) => {
    this.setState({user: e.target.value})
  }

  handleChangePassword = (e) => {
    this.setState({password: e.target.value})
  }

  handleChangeUserRegis = (e) => {
    this.setState({regisUser: e.target.value})
  }

  handleChangePasswordRegis = (e) => {
    this.setState({regisPassword: e.target.value})
  }

  handleSubmitRegis = () => {
    const name = this.state.regisUser;
    const password = this.state.regisPassword;
    const result = users({name: name}).first()

    if (result === false && name !== '' && password !== '') {
      users.insert({name, password});

    } else {
      this.setState({error: true})
    }
  }

  handleSubmit = () => {
    const result = users({name: this.state.user, password: this.state.password}).first()

    if (result !== false) {
      login(result)
      this.props.history.push("/")
      
    } else {
      this.setState({error: true})
    }
  }

  render() {
    const user = this.state.user
    const password = this.state.password
    const regisUser = this.state.regisUser
    const regisPassword = this.state.regisPassword
    const error = this.state.error
    //const { user, password , regisUser, regisPassword, error } = this.state
    return (
      <div>
        <Grid >
          <Grid.Row columns={2}>
            <Grid.Column style={{paddingLeft:16}}>
            <h2>Login</h2>
              <Segment size="huge">
                <Form onSubmit={this.handleSubmit}>
                  <Form.Field>
                    <Form.Input
                      icon="user"
                      iconPosition="left"
                      label="User Name"
                      labelPosition="right"
                      value={user}
                      onChange={this.handleChangeUser}/>
                  </Form.Field>
                  <Form.Field>
                    
                    <Form.Input
                      icon="lock"
                      iconPosition="left"
                      type="password"
                      label="Password"
                      value={password}
                      onChange={this.handleChangePassword}/>
                    <Divider/>
                  </Form.Field>
                  <Button color="green" fluid type="submit">
                    Login
                  </Button>
                </Form>
              </Segment>
            </Grid.Column>

            <Grid.Column style={{paddingLeft:16}}>
            <h2>Registration</h2>
              <Segment size="huge">
                <Form onSubmit={this.handleSubmitRegis}>
                  <Form.Field>
                    <Form.Input
                      icon="user"
                      iconPosition="left"
                      label="User Name"
                      labelPosition="right"
                      value={regisUser}
                      onChange={this.handleChangeUserRegis}/>
                  </Form.Field>
                  <Form.Field>
                    <Form.Input
                      icon="lock"
                      iconPosition="left"
                      type="password"
                      label="Password"
                      value={regisPassword}
                      onChange={this.handleChangePasswordRegis}/>
                    <Divider/>
                  </Form.Field>
                  <Button color="green" fluid type="submit">
                    Registrate
                  </Button>
                </Form>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          {error === true ? 
          <Grid.Row textAlign="center">
            <Segment color="red" inverted>
              <h3>Anscheinend haben Sie was falsch eingegeben. Bitte versuchen Sie es erneut!</h3>
            </Segment>
          </Grid.Row> : null}
          <Button color="red" onClick={() => logout()}>Logout</Button>
        </Grid>
      </div>
    );
  }
}

export default Login;