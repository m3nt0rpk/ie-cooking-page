import React from 'react';
import './App.css';
import {Grid, Segment, Image, Rating} from "semantic-ui-react";
import PizzaImg from './pizza1.png'

const InitialRecipe = () => {
  return(
  <div>
    <Segment inverted color="green">
      <Grid>
        <Grid.Row>
          <Grid.Column width={6} style={{marginLeft:28}}>        
            <h1>Pizza Prociutto e Funghi</h1>         
          </Grid.Column >
          <Grid.Column width={5}>
            <h2>von Kranthi Mailaram</h2>
            </Grid.Column>
          <Grid.Column width={3}>              
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={8}>
            <Segment inverted color="yellow" style={{marginLeft:28}} className="imageSegment">
              <Image src={PizzaImg} className="imageWidth"/>
                <Rating maxRating={5} defaultRating={5} icon='heart' size="massive" className="ratingPos" />
            </Segment>
          </Grid.Column>
          <Grid.Column width={8}>
            <Segment inverted color="black">
            <h2>Zutaten:</h2>
              <ul>
                <li>200g Pizzateig</li>
                <li>70ml Tomatenssauce</li>
                <li>70g Mozarella, gerieben</li>
                <li>60g Kochschinken</li>
                <li>40g Champignonscheiben</li>
              </ul>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment inverted color="black" style={{marginLeft:28}}>
              <h2>Zubereitung:</h2>
                <ol>
                  <li>Drehe den Teigling aus und verteile 70ml Tomatensauce gleichmäßig von innen nach außen darauf, sodass 1cm Rand frei bleibt. Gib 70 g Mozarella gleichmäßig von außen nach innen gebröselt darüber, sodass die Pizza bis auf 1cm Rand gleichmäßig bedeckt ist.</li>
                  <li>Verteile dann 60g geviertelten Kochschinken und 40g Champignons auf der ganzen Pizza. Schiebe die Pizza in den ofen und wende sie mindestens einmal, sodass sie gleichmäßig backt und bräunt.</li>
                  <li>Wenn die Pizza fertig gebacken ist, platziere die Pizza auf einen Telle und scheide sie in 8 gleich große Stücke.</li>
                </ol>
            </Segment>
            </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  </div>
)   
}

export default InitialRecipe;