import React, { Component } from 'react';
import './App.css';
import {Rating, Item, Segment, Button } from "semantic-ui-react"
import DefaultImage from './food-placeholder.png';
import { Link } from 'react-router-dom';
import { getUser } from './auth'

const TAFFY = require("taffydb").taffy;

export const users = TAFFY([
  {
    name: 'pawel',
    password: '1234'
  }
])

const avgRating = item => {
  let ratingSum = Object.keys(item.ratings)
  .map(key => item.ratings[key])
  .reduce((sum, next) => sum + next, 0);

  let ratingDivisor = 
  Object.keys(item.ratings)
  .map(key => item.ratings[key]).length;

  return Math.round(ratingSum / ratingDivisor);
}

class App extends Component {
  constructor(props){
    super(props)
    this.state={
      count:0,
      rating:0
    };
  }

  handleRating = (rating, item) => {
    const user = getUser()
    if (!user) {
      return;
    }
    const name = user.name;
    const newRatings = {...item.ratings, [name]: rating};
    const newItem = {...item, ratings: newRatings};
    this.props.updateRecipe(newItem);
  }
  
  handleSwap = () => {
    const recipes = this.props.recipes;
    const swapedArray = recipes.reverse();
    this.setState({swapedArray});
  }

  render() {
    const url = this.props.match.url;
    const user = getUser();
    const userName = user && user.name;
    let filter = null;

    if(url === "/your-recipes"){
      filter = item => {
        if (item) {
          return item.user === userName;
        } else {
          return false;
        }
      }
    }
    else if (url === "/best-recipes"){
      filter = item => avgRating(item) >= 4
    }
    else {
      filter = item => true;
    }
      const length = Object.keys(this.props.recipes).length;
    return (
    <div>
        <Segment color="yellow" inverted>
        <Item.Group divided>
            {this.props.recipes.filter(filter).map((item, idx) => (
              <Item key={idx}>
              <Item.Image size="small" src={item.img !== '' ? item.img : DefaultImage} alt="Image" as={Link} to={`/recipe/${item.slug}`}/>
                <Item.Content>
                  <Item.Header>{item.name}</Item.Header>
                  <Item.Description><div>{item.prep[0]}..</div></Item.Description>
                  <Item.Extra>
                    <p>von {item.user}</p>
                  </Item.Extra>
                    <Item.Extra>
                      Voted:{Object.keys(item.ratings).map(key => item.ratings[key]).length}
                    </Item.Extra>
                    <Item.Extra>
                    <Rating maxRating={5} size="huge" rating={avgRating(item)} icon='heart' onRate={(e, {rating}) => this.handleRating(rating, item)}/>
                    </Item.Extra>
                </Item.Content>
              </Item>
            ))}
        </Item.Group>
        </Segment>
        {length >= 2 ? <Button onClick={this.handleSwap} color="green">Swap Order</Button> : null}
      </div>
    );
  }
}

export default App;
