import React, {Component} from 'react';
import './App.css';
import {Segment, Form, Button} from "semantic-ui-react"
import { getUser } from "./auth"

class CreateRecipe extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      img: '',
      ing: '',
      prep: '',
    }
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const {name, ing, prep, img} = this.state

    if (name === '' || ing === '' || prep === '') {
      return;
    }
    const ingredientsList = ing.split('\n');
    const preparationList = prep.split('\n');
    const slug = name.toLowerCase().split(' ').join('-')
    const user = getUser().name
    const data = {
      name,
      ing: ingredientsList,
      prep: preparationList,
      img,
      slug,
      ratings: {},
      user
    };

    this.props.addItemToList(data);
    this.setState({ name: '', ing: '', prep: '', img: ''})
    this.props.history.push(`/recipe/${slug}`)
  }

  render() {
    const {name, ing, prep, img } = this.state;
    return (
      <Segment>
        <Form onSubmit={this.handleSubmit}>
          <Form.Input
            required
            label="Recipe Name"
            value={name}
            name="name"
            onChange={this.handleChange}/>
          <Form.TextArea
            required
            label="Ingrediants"
            value={ing}
            name="ing"
            onChange={this.handleChange}/>
          <Form.TextArea
            required
            label="Preparation"
            value={prep}
            name="prep"
            onChange={this.handleChange}/>
          <Form.Input
            label="Link Image"
            value={img}
            name="img"
            onChange={this.handleChange}
            placeholder="http://via.placeholder.com/300x300"/>
          <Button type="submit" color="green">Submit</Button>
        </Form>
      </Segment>
    );
  }
}

export default CreateRecipe;