import React, {Component} from 'react';
import './App.css';
import { Segment, Form, Button} from "semantic-ui-react"
import { findRecipe } from "./RecipeLayout"

class RecipeEditor extends Component {
  constructor(props) {
    super(props)

    const recipe = findRecipe(props.recipes, props.match.params.name);
    const prep = recipe.prep.join('\n');
    const ing = recipe.ing.join('\n');
    this.state = {
      ...recipe,
      prep,
      ing
    }
  }
  
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {name, ing, prep, img} = this.state

    if (name === '' || ing === '' || prep === '') {
      this.setState({submitted: false})
      return;
    }
    const ingredientsList = ing.split('\n');
    const preparationList = prep.split('\n');
    const slug = name
      .toLowerCase()
      .split(' ')
      .join('-')
    const data = {
      name,
      ing: ingredientsList,
      prep: preparationList,
      img,
      slug,
      ratings: this.state.ratings,
      user: this.state.user
    };
    this.props.updateRecipe(data)
    this.props.history.push(`/recipe/${slug}`)
  }

  render() {
    const {name, ing, prep, img } = this.state;
    return (
      <Segment>
        <Form onSubmit={this.handleSubmit}>
          <Form.Input
            required
            label="Recipe Name"
            value={name}
            name="name"
            onChange={this.handleChange}/>
          <Form.TextArea
            required
            label="Ingrediants"
            value={ing}
            name="ing"
            onChange={this.handleChange}/>
          <Form.TextArea
            required
            label="Preparation"
            value={prep}
            name="prep"
            onChange={this.handleChange}/>
          <Form.Input
            label="Link Image"
            value={img}
            name="img"
            onChange={this.handleChange}
            placeholder="http://via.placeholder.com/300x300"/>
          <Button type="submit" color="green">Submit</Button>
        </Form>
      </Segment>
    );
  }
}

export default RecipeEditor;