import React, {Component} from 'react';
import './App.css';
import {Menu} from "semantic-ui-react"
import { Link } from "react-router-dom"

class MenuBar extends Component {
  constructor(props) {
    super(props)
    this.state = {};
  }
  handleItemClick = (e, {name}) => this.setState({activeItem: name})

  render() {
    const {activeItem} = this.state
    return (
      <Menu widths={6} color="red" inverted>
        <Menu.Item
          name='allRecipes'
          active={activeItem === 'allRecipes'}
          onClick={this.handleItemClick}
          as={Link}
          to='/'>
          <p className="white">All Recipes</p>
        </Menu.Item>

        <Menu.Item
          name='bestRecipes'
          active={activeItem === 'bestRecipes'}
          onClick={this.handleItemClick}
          as={Link}
          to='/best-recipes'>
          <p className="white">Best Recipes</p>
        </Menu.Item>

        <Menu.Item
          name='yourRecipes'
          active={activeItem === 'yourRecipes'}
          onClick={this.handleItemClick}
          as={Link}
          to="/your-recipes">
          <p className="white">Your Recipes</p>
        </Menu.Item>

        <Menu.Item
          name='createRecipe'
          active={activeItem === 'createRecipe'}
          onClick={this.handleItemClick}
          as={Link}
          to="/create-recipe">
          <p className="white">Create Recipe</p>
        </Menu.Item>
        
        <Menu.Item
          name='contact'
          active={activeItem === 'contact'}
          onClick={this.handleItemClick}
          as={Link}
          to="/contact">
          <p className="white">Contact</p>
        </Menu.Item>

        <Menu.Item
          name='login'
          active={activeItem === 'login'}
          onClick={this.handleItemClick}
          as={Link}
          to="/login">
          <p className="white">Login/&shy;Registration</p>
        </Menu.Item>
      </Menu>
    );
  }
}

export default MenuBar;