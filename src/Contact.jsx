import React from 'react';
import './App.css';
import {Segment} from "semantic-ui-react"

const Contact = () => {
  return (
    <div>
      <Segment>
        <h2>Contact us!!!</h2>
        <p>Problems with creating or submitting recipes?</p>
        {/*https://www.christoph-freyer.at/blog/links-absichern-mit-rel-noopener/#.Wyjt36f-iUk*/}
        <p>Then please send us an <a target="_blank" rel="noopener noreferrer" href="mailto:kranthi.mailaram@gmx.at?cc=ic16b027@technikum-wien.at&bcc=ic17b044@technikum-wien.at">Email</a>!
        </p>
      </Segment>
    </div>
  );
}

export default Contact;